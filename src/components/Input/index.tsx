import {StyleSheet, Text, TextInput, View} from 'react-native';
import React from 'react';
import {colors, fonts} from '@utils/Style';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

interface InputProps {
  label: string;
  placholder: string;
  onChange?: () => void;
  value?: string;
  secureTextEntry?: boolean;
}

const Input: React.FC<InputProps> = ({label, placholder, onChange, value, secureTextEntry = false}) => {
  const [_border, setBorder] = React.useState(colors.black);
  const onFocusForm = () => {
    setBorder(colors.secondary);
  };
  const onBlurForm = () => {
    setBorder(colors.black);
  };
  return (
    <View style={styles.page}>
      <Text style={styles.label}>{label}</Text>
      <View style={[styles.conInp, {borderColor: _border}]}>
        <TextInput
          placeholder={placholder}
          style={styles.inp}
          onChangeText={onChange}
          value={value}
          onFocus={onFocusForm}
          onBlur={onBlurForm}
          secureTextEntry={secureTextEntry}
        />
      </View>
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  page: {marginHorizontal: wp(3), marginBottom: hp(2)},
  label: {fontSize: 16, fontFamily: fonts.regular, color: colors.text.primary, marginBottom: hp(1)},
  conInp: {borderWidth: 1, borderRadius: 8},
  inp: {fontSize: 14, fontFamily: fonts.regular, paddingHorizontal: wp(3)},
});
