import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {Left} from '@asset/Icon';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {colors, fonts} from '@utils/Style';
import {Pic} from '@asset/Img';

interface HeadrProps {
  Back: boolean;
  Profile: boolean;
}

const Header: React.FC<HeadrProps> = ({Back, Profile}) => {
  return (
    <View style={styles.page}>
      {Back && (
        <TouchableOpacity style={styles.back}>
          <Left height={hp(3.5)} width={wp(4)} />
        </TouchableOpacity>
      )}
      <View style={styles.conText}>
        <Text style={styles.txtHead}>Sign In</Text>
        <Text style={styles.txtSubtitel}>Find your best ever meal</Text>
      </View>
      {Profile && (
        <View style={styles.conImg}>
          <Image source={Pic} style={styles.img} />
        </View>
      )}
    </View>
  );
};

export default Header;
const styles = StyleSheet.create({
  page: {flexDirection: 'row', alignItems: 'center', paddingHorizontal: wp(3)},
  back: {paddingRight: wp(5)},
  conText: {flex: 1},
  txtHead: {fontSize: 22, fontFamily: fonts.medium, color: colors.text.primary},
  txtSubtitel: {fontSize: 14, fontFamily: fonts.light, color: colors.text.subtitel},
  conImg: {width: wp(20), alignItems: 'flex-end'},
  img: {width: wp(13), height: wp(13), borderRadius: wp(2)},
});
