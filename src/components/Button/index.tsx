import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors, fonts} from '@utils/Style';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

interface ButtonProps {
  label: string;
  onPress?: () => void;
  type: 'disable' | 'enable' | 'red';
}

const Button: React.FC<ButtonProps> = ({label, onPress, type = 'disable'}) => {
  const TypeFun = () => {
    switch (type) {
      case 'disable':
        return colors.grey;
      case 'enable':
        return colors.primary;
      case 'red':
        return colors.red;
      default:
        return colors.grey;
    }
  };
  if (type === 'disable') {
    return (
      <View style={[styles.page, {backgroundColor: TypeFun()}]}>
        <Text style={[styles.tlBtn, {color: TypeFun() === colors.primary ? colors.text.primary : colors.text.white}]}>
          {label}
        </Text>
      </View>
    );
  }

  return (
    <TouchableOpacity style={[styles.page, {backgroundColor: TypeFun()}]} onPress={onPress}>
      <Text style={[styles.tlBtn, {color: TypeFun() === colors.primary ? colors.text.primary : colors.text.white}]}>
        {label}
      </Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  page: {
    paddingVertical: wp(3),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: wp(2),
    marginHorizontal: wp(3),
    marginBottom: hp(2),

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  tlBtn: {fontFamily: fonts.medium, fontSize: 14},
});
