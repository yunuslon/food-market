import {StyleSheet, View} from 'react-native';
import React from 'react';
import NavigationRoot from '@nav/index';
import {Provider, useSelector} from 'react-redux';
import store from '@redux/store';
import FlashMessage from 'react-native-flash-message';
import {Loading} from '@component/index';
import {GlobalState} from '@redux/staterReducer/global';

interface AppState {
  globalReducer: GlobalState;
}

const MainApp = () => {
  const {isLoading} = useSelector((state: AppState) => state.globalReducer);
  console.log('isLoading', isLoading);

  return (
    <View style={styles.page}>
      <FlashMessage position="top" />
      <NavigationRoot />
      {isLoading && <Loading />}
    </View>
  );
};

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <MainApp />
    </Provider>
  );
};

export default App;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
});
