const mainColor = {
  primary: '#FFC700',
  secondary: '#1ABC9C',
  grey: '#8D92A3',
  white: '#F9FAFF',
  black: '#020202',
  red: '#D9435E',
};

export const colors = {
  primary: mainColor.primary,
  secondary: mainColor.secondary,
  red: mainColor.red,
  grey: mainColor.grey,
  white: mainColor.white,
  black: mainColor.black,
  text: {
    primary: mainColor.black,
    subtitel: mainColor.grey,
    white: mainColor.white,
    toska: mainColor.secondary,
  },
};

export const fonts = {
  medium: 'Poppins-Medium',
  regular: 'Poppins-Regular',
  light: 'Poppins-Light',
};
