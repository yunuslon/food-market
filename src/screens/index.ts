import Login from './Login';
import Register from './Register';
import Splash from './Splash';
import Home from './Home';

export {Login, Register, Splash, Home};
