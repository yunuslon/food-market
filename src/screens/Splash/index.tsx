import {Text, StyleSheet, StatusBar} from 'react-native';
import React, {useEffect} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {colors, fonts} from '@utils/Style';
import {Home} from '@asset/Svg';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import routes from '@nav/routes';
import {useNavigation} from '@react-navigation/native';

type SplashScreenProp = NativeStackNavigationProp<routes, 'LOGIN'>;

const Splash: React.FC = () => {
  const navigation = useNavigation<SplashScreenProp>();

  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('LOGIN');
    }, 2000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <SafeAreaView style={styles.page}>
      <StatusBar translucent backgroundColor="transparent" />
      <Home />
      <Text style={styles.txt}>FoodMarket</Text>
    </SafeAreaView>
  );
};

export default Splash;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.primary,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt: {fontSize: 32, color: colors.text.primary, fontFamily: fonts.medium, marginTop: hp(1)},
});
