import {Header, Input, Button, Gap} from '@component/index';
import {colors} from '@utils/Style';
import React from 'react';
import {ScrollView, StatusBar, StyleSheet} from 'react-native';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {SafeAreaView} from 'react-native-safe-area-context';

const Login: React.FC = () => {
  return (
    <SafeAreaView style={styles.page}>
      <StatusBar translucent backgroundColor="transparent" barStyle={'dark-content'} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <Header Back={true} Profile={false} />
        <Gap height={5} />
        <Input label="Full Name" placholder="Type your full name" />
        <Input label="Password" placholder="Type your Password" secureTextEntry />
        <Gap height={5} />
        <Button label="Sign In" type={'enable'} onPress={() => {}} />
        <Button label="Create New Account" type={'disable'} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Login;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
    paddingVertical: hp(2),
    paddingHorizontal: wp(2),
  },
});
